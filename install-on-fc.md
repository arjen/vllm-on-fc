# Experiments on Tau

## Preliminaries

### CUDA

Not quite sure... but it seems that using `nvcc` requires going through rpm-fusion route.
Following the [NVIDIA on RPM-Fusion docs](https://rpmfusion.org/Howto/CUDA#CUDA_Toolkit):

    sudo dnf config-manager --add-repo https://developer.download.nvidia.com/compute/cuda/repos/fedora37/x86_64/cuda-fedora37.repo
    sudo dnf clean all
    sudo dnf module disable nvidia-driver
    sudo dnf -y install cuda

Possibly also need [NVIDIA docs about Linux installation](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/#meta-packages).

### Python

PyTorch support for CUDA 12.3 seems to be only available for Python 3.10 (based on [NVIDIA's framework matrix](https://docs.nvidia.com/deeplearning/frameworks/support-matrix/index.html)).

    sudo dnf install virtualenv python3.10

Create a virtualenv to ease local install/configuration:

    virtualenv -p 3.10 .venv
    source .venv/bin/activate

Install `pip`:

    python -m ensurepip --upgrade

Install `pytorch` (following [pytorch docs](https://pytorch.org/get-started/locally/)):

### Podman

#### Podman rootless setup

Maybe more needed from [podman rootless tutorial](https://github.com/containers/podman/blob/main/docs/tutorials/rootless_tutorial.md).

    # sudo dnf install slirp4netns 
    sudo usermod --add-subuids 100000-165535 --add-subgids 100000-165535 $USER

#### Local container storage

We have to ensure containers are not stored on a network drive, NFS is too slow and actually buggy too.

Create a storage location:

    mkdir -p /data/${USER}/containers/storage
    sudo  semanage fcontext -a -e /var/lib/containers/storage /data/${USER}/containers/storage

Edit `$HOME/.config/containers/storage.conf` to contain:
```
[storage]
driver = "btrfs"
rootless_storage_path = "/data/${USER}/containers/storage"
```

Update configuration

    podman system reset

Check the modified setup using `podman info`.

#### NVIDIA related configuration

NVIDIA Container Toolkit:

    curl -s -L https://nvidia.github.io/libnvidia-container/stable/rpm/nvidia-container-toolkit.repo |   sudo tee /etc/yum.repos.d/nvidia-container-toolkit.repo
    sudo dnf install -y nvidia-container-toolkit

NVIDIA Container Device Interface (recommended for `podman`):

    # Generate YAML files for CDI
    sudo nvidia-ctk cdi generate --output=/etc/cdi/nvidia.yaml
    sudo nvidia-ctk cdi generate --output=/var/run/cdi/nvidia.yaml

    # maybe not needed any more...
    sudo chmod a+r /etc/cdi/nvidia.yaml /var/run/cdi/nvidia.yaml

Check for success using `nvidia-ctk cdi list`.    
Also check that the following two commands give the same output:

    nvidia-smi -L
    podman run --rm --device nvidia.com/gpu=all --security-opt=label=disable ubuntu nvidia-smi -L

Getting errors that `/dev/nvidia-uvm` cannot be found... a workaround is to run this shell-script as root:

    sudo nvidia-uvm-init.sh

The script is taken from this blog post on [NVIDIA GPU passthrough](https://matthieu.yiptong.ca/2020/12/06/nvidia-gpu-passthrough-to-lxc-containers-on-proxmox-6-for-nvenc-in-plex/). Let's indeed install it for initialization at boot time, as suggested in the post:

    sudo cp nvidia-uvm-init.service /etc/systemd/system
    sudo cp nvidia-persistenced.service /etc/systemd/system

Finalize the `systemd` setup for running these at boot:

    sudo systemctl daemon-reload
    sudo systemctl start nvidia-uvm-init.service
    sudo systemctl start nvidia-persistenced.service 

    sudo systemctl enable nvidia-uvm-init.service
    sudo systemctl enable nvidia-persistenced.service 


