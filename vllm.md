# Experiments with vLLM

## Preliminaries

### NVIDIA dependencies

Make sure that the machine is setup for using NVIDIA GPU tools and containers.
Hereto, follow the steps in the [Fedora Core setup documentation](install-on-fc.md) first.

### Podman containers

Create and start a podman container for using vLLM:
    
    podman create -it -v /data/${USER}/vllm-storage:/data:Z --replace --name vllm --cgroup-conf=memory.high=28g --device nvidia.com/gpu=all nvcr.io/nvidia/pytorch:23.10-py3
    podman start vllm

The vLLM documents suggested to add `--ipc=host` but that turned out problematic when containers ran out of memory.
Problems due to exhausting memory were resolved by adding a 32G swapfile 
([instructions for BTRFS](https://btrfs.readthedocs.io/en/latest/Swapfile.html), check with `swapon --show`).

The remainder of this document assumes that we enter a shell in this container:

    podman exec -it vllm /bin/bash

### Installation vLLM

First, clone the repository (check if necessary, it may already exist on volume share `/data/${USER}/vllm-storage`):

    cd /data
    git clone https://github.com/vllm-project/vllm.git
    cd vllm

Build vLLM (_this requires rather excessive memory!_):

    pip install -e .

After a long time and a lot of swapping, hopefully this completes successfully and you are ready to use the LLMs supported.

## Using vLLM

Example file: `my-ex.py`.

Notes:

+ Default configuration stores loaded models in `/root/.cache/huggingface`. Since these take ages to load, we should place these on the volume share. Use the `download_dir` parameter when creating the `LLM` object.

### LLAMA 2

You need to request access through HuggingFace after getting approval by Meta. 
Create a token on HuggingFace and provide it before using vLLM:

    huggingface-cli login --token `cat /data/vllm-models/.hftoken.txt`

(Need a better way - now it's tied to my personal account!)

## Documentation

Created using the instructions within the container - but written to the shared filesystem.

    cd /data/${USER}/vllm-storage/vllm/docs/
    python -m http.server build/html/


