# vllm-on-fc

This repository tracks the process of installing vLLM on a Linux Fedora Core 39 system 
with an NVIDIA GeForce RTX 3090 GPU.

Check [install-on-fc](install-on-fc.md) for the required steps to get the OS prepared.

Check [vllm](vllm.md) to build and use [vLLM](https://docs.vllm.ai/).

